package com.otssso.samimchala.gumtree.presentation.presenter;

import com.otssso.samimchala.gumtree.presentation.model.Advert;

/**
 * Created by samimchala on 09/04/2017.
 */

public interface AdvertPresenter {
    Advert getAdvert();
    void unSubscribe();
}
