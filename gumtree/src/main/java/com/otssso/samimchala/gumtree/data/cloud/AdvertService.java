package com.otssso.samimchala.gumtree.data.cloud;

import com.otssso.samimchala.gumtree.domain.IAdvertService;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

import static com.otssso.samimchala.gumtree.data.cloud.Configuration.ADVERT_BASE_URL;
import static com.otssso.samimchala.gumtree.data.cloud.Configuration.ADVERT_END_POINT;
import static com.otssso.samimchala.gumtree.data.cloud.Configuration.ADVERT_ID;
import static com.otssso.samimchala.gumtree.data.cloud.OKHTTPClient.getClient;

/**
 * Created by samimchala on 09/04/2017.
 */

public class AdvertService implements IAdvertService {
    private AdvertApi advertApi;

    public AdvertService() {
        this(ADVERT_BASE_URL);
    }

    public AdvertService(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        advertApi = retrofit.create(AdvertApi.class);
    }

    public AdvertApi getAdvertApi() {
        return advertApi;
    }

    public interface AdvertApi {
        @GET(ADVERT_END_POINT)
        Observable<Response<ResponseBody>> getAdvertRequest();
    }
}
