package com.otssso.samimchala.gumtree.data.model;

/**
 * Created by samimchala on 09/04/2017.
 */

public class AdvertResponseError {
    String message;
    int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
