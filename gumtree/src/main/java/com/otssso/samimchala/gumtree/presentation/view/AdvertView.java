package com.otssso.samimchala.gumtree.presentation.view;

import com.otssso.samimchala.gumtree.presentation.model.Advert;

/**
 * Created by samimchala on 09/04/2017.
 */

public interface AdvertView {
    void showError(String error);
    void showAdvert(Advert responseBody);
}
