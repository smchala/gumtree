package com.otssso.samimchala.gumtree.data.cloud;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class OKHTTPClient {
    public static OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(Configuration.TIME_OUT_FIVE_MINUTES, TimeUnit.MINUTES)
                .readTimeout(Configuration.TIME_OUT_FIVE_MINUTES, TimeUnit.MINUTES)
                .build();
        return client;
    }
}
