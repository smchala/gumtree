package com.otssso.samimchala.gumtree.presentation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.otssso.samimchala.gumtree.R;
import com.otssso.samimchala.gumtree.presentation.model.Advert;
import com.otssso.samimchala.gumtree.presentation.presenter.AdvertPresenter;
import com.otssso.samimchala.gumtree.presentation.presenter.AdvertPresenterImpl;

public class AdvertActivity extends AppCompatActivity implements AdvertView{

    private AdvertPresenter advertPresenter;
/*
Should have started with a splash that would have gotten the advert and passed it as a extra parcelable
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advert_layout);
        setToolBar("chair");

        advertPresenter = new AdvertPresenterImpl(this);
        advertPresenter.getAdvert();

        ((LinearLayout)findViewById(R.id.location_layout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("sm", "location_layout CLICKED!!!!");
            }
        });

        ((LinearLayout)findViewById(R.id.user_layout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("user_layout sm", "CLICKED!!!!");
            }
        });

        setMap("51.416040, -0.753980");
    }

    private void setMap(String longLat) {
        ImageView imageView = (ImageView)findViewById(R.id.static_map);
        Glide.with(this)
                .load(getGoogleStaticMapUrl(longLat))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.imagenotfound)
                .into(imageView);
    }

    //to be moved in Domain layer
    @NonNull
    private String getGoogleStaticMapUrl(String longLat) {
        //should also set the size for landscape
        return "https://maps.googleapis.com/maps/api/staticmap?center="+longLat+"&markers=color:green%7C"+longLat+"&zoom=15&scale=2&size=320x200&key="+getString(R.string.google_android_static_map_api_key);
    }

    private void setToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        setViewPager();
        setTitleWhenCollapsedToolBar(title);

    }

    private void setViewPager() {
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPageAndroid);
        AndroidImageAdapter adapterView = new AndroidImageAdapter(this);
        mViewPager.setAdapter(adapterView);
        mViewPager.setOffscreenPageLimit(9);//get the number of images per advert
    }

    private void setTitleWhenCollapsedToolBar(final String title) {
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(title);
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void showError(String error) {
        Log.d("sm", "+++++++   ERROR "+error);
    }

    @Override
    public void showAdvert(Advert advert) {
//        setViewPager(advert.getImages());
//        setTitleWhenCollapsedToolBar(advert.getTitle());
    }

    @Override
    protected void onDestroy() {
        advertPresenter.unSubscribe();
        super.onDestroy();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);

        setShareAction(menu);

        return true;
    }

    private void setShareAction(Menu menu) {
        MenuItem shareItem = menu.findItem(R.id.action_share);
        ShareActionProvider myShareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        Intent myShareIntent = new Intent(Intent.ACTION_SEND);
        myShareIntent.setType("*/*");
        myShareIntent.putExtra(Intent.EXTRA_TEXT, "http://samimchala.co.uk/sc/gumtree/chair.json");
        myShareActionProvider.setShareIntent(myShareIntent);
    }
}
