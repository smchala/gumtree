package com.otssso.samimchala.gumtree.presentation.view;

/**
 * Created by samimchala on 06/04/2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.otssso.samimchala.gumtree.R;

import java.util.List;

public class AndroidImageAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    Context mContext;
    private ImageView imageView;
    private String[] sliderImagesUrls = new String[]{
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer9.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer8.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer7.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer6.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer5.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer4.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer3.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer2.png",
            "http://www.samimchala.co.uk/sc/shops/offersressources/offer1.png"
    };

    AndroidImageAdapter(Context context) {
        this.mContext = context;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sliderImagesUrls.length;
    }

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int i) {
        View itemView = layoutInflater.inflate(R.layout.view_pager_layout, container, false);

        imageView = (ImageView) itemView.findViewById(R.id.image_view);
        Glide.with(mContext)
                .load(sliderImagesUrls[i])
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.imagenotfound)
                .into(imageView);

        imageView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Log.i("sm", "This page was clicked: " + i);
            }
        });

        ((ViewPager) container).addView(imageView,0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }
}