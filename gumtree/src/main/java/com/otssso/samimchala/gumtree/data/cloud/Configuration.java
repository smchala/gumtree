package com.otssso.samimchala.gumtree.data.cloud;

/**
 * Created by samimchala on 09/04/2017.
 */

class Configuration {
    public static final long TIME_OUT_FIVE_MINUTES = 5;
    public static final String ADVERT_BASE_URL = "http://samimchala.co.uk/sc/gumtree/";
    public static final String ADVERT_END_POINT = "chair.json";
    public static final String ADVERT_ID = "advert_id";
}
