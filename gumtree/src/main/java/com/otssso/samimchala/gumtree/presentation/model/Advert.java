package com.otssso.samimchala.gumtree.presentation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
/**
 * Created by samimchala on 06/04/2017.
 */

/*
{
   "advert":{
      "images":[
         "url1",
         "url2",
         "url3"
      ],
      "title":"chair",
      "location":{
         "city":"Bracknell",
         "country":"UK",
         "long":51.416040,
         "lat":-0.753980
      },
      "price":{
         "value":25,
         "currency":"GBP"
      },
      "dateposted":"April 7, 2017",
      "description":"nice chair",
      "id":12345,
      "member":{
         "numberofliveads":5,
         "name":"Sami",
         "email":"test@test.com",
         "membersince":"July 17, 2016"
      }
   }
}
*/

public class Advert{

    @SerializedName("images")
    @Expose
    private List<String> images = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("dateposted")
    @Expose
    private String dateposted;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("member")
    @Expose
    private Member member;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getDateposted() {
        return dateposted;
    }

    public void setDateposted(String dateposted) {
        this.dateposted = dateposted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}

