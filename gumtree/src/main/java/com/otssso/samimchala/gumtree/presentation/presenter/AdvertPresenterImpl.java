package com.otssso.samimchala.gumtree.presentation.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.otssso.samimchala.gumtree.data.cloud.AdvertService;
import com.otssso.samimchala.gumtree.data.model.AdvertResponseError;
import com.otssso.samimchala.gumtree.presentation.model.Advert;
import com.otssso.samimchala.gumtree.presentation.view.AdvertView;

import java.io.InputStreamReader;
import java.io.Reader;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by samimchala on 09/04/2017.
 */

public class AdvertPresenterImpl implements AdvertPresenter {
    private final AdvertView advertView;
    private AdvertService.AdvertApi advertApi;
    private Subscription subscription;
    private Gson gson;

    public AdvertPresenterImpl(AdvertView advertView) {
        this.advertView = advertView;
    }

    @Override
    public Advert getAdvert() {
        gson = new Gson();
        AdvertService advertService = new AdvertService();
        advertApi = advertService.getAdvertApi();

        final Observable<Response<ResponseBody>> advert = advertApi.getAdvertRequest();

        subscription = advert.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        Log.d("sm",":( ERROR: " +e.toString());
                    }

                    @Override
                    public void onNext(Response<ResponseBody> advertResponse) {
                        if (advertResponse.code() == 200){

                            Reader reader = new InputStreamReader(advertResponse.body().byteStream());
                            Advert responseBody = gson.fromJson(reader, Advert.class);

                            advertView.showAdvert(responseBody);
                        }else  if(advertResponse.errorBody() != null){

                            Reader reader = new InputStreamReader(advertResponse.errorBody().byteStream());
                            AdvertResponseError response = gson.fromJson(reader, AdvertResponseError.class);
                            advertView.showError(response.getMessage());
                        }
                    }
                });
        return null;
    }

    @Override
    public void unSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}
