package com.otssso.samimchala.gumtree.presentation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samimchala on 09/04/2017.
 */

public class Member {

    @SerializedName("numberofliveads")
    @Expose
    private Long numberofliveads;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("membersince")
    @Expose
    private String membersince;

    public Long getNumberofliveads() {
        return numberofliveads;
    }

    public void setNumberofliveads(Long numberofliveads) {
        this.numberofliveads = numberofliveads;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMembersince() {
        return membersince;
    }

    public void setMembersince(String membersince) {
        this.membersince = membersince;
    }

}
